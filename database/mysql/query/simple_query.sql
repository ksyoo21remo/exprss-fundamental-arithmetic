-- 
-- 회원가입 직후 설문조사에서 입력한 통증 부위에 적합한 운동 추천하기
--
SELECT
  M.name AS name,
  S.id AS survey_id,
  E.name_kr AS recommended_exercise
FROM
  musers M
  INNER JOIN surveys_simple S ON M.id = S.muser_id
  INNER JOIN exercises E ON S.painarea = E.body_part
WHERE
  M.name LIKE '%son%'
ORDER BY
  M.name,
  E.id;
