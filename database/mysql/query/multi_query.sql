-- 
-- 회원가입 직후 설문조사에서 입력한 `여러 개의` 통증 부위에 적합한 운동 추천하기
-- surveys 테이블 외에 pain_areas 라는 테이블 별도로 만들어서 관리
--
SELECT
	PA.muser_id,
    PA.pain_area,
    EX.body_part,
    EX.id,
    EX.name_en,
    EX.name_kr
FROM pain_areas PA
	INNER JOIN (
	SELECT
		body_part,
		id,
		name_en,
		name_kr
	FROM
		exercises
	) EX ON PA.pain_area = EX.body_part
ORDER BY
	PA.muser_id,
	EX.id
;

-- JSON 타입의 컬럼 있는 경우에

UPDATE surveys_with_json
SET pain_areas = (
	SELECT result
	FROM (
		SELECT JSON_REPLACE(pain_areas, '$[0]', 1) AS result
		FROM surveys_with_json
		WHERE id = 1
	) AS J
)
WHERE id = 1
;

SELECT JSON_REPLACE(pain_areas, '$[0]', 2) AS result
FROM surveys_with_json
WHERE id = 1
;

SELECT JSON_SET(pain_areas, '$[2]', 10) AS result
FROM surveys_with_json
WHERE id = 1
;

UPDATE surveys_with_json
SET pain_areas = (
	SELECT result
	FROM (
		SELECT JSON_REMOVE(pain_areas, '$[3]') AS result
		FROM surveys_with_json
		WHERE id = 1
	) AS J
)
WHERE id = 1
;

SELECT JSON_REMOVE(pain_areas, '$[3]') AS result
FROM surveys_with_json
WHERE id = 1
;

SELECT
	id,
    muser_id,
    pain_areas,
	pain_areas->'$[0]',
	pain_areas->'$[1]'
FROM 
	surveys_with_json
;

SELECT 
	S.muser_id,
    S.pain_areas,
    E.body_part,
    E.id,
    E.name_en,
    E.name_kr
FROM
	surveys_with_json S
	INNER JOIN exercises E ON JSON_CONTAINS(S.pain_areas, JSON_ARRAY(E.body_part))
ORDER BY
	S.muser_id,
    E.id
;
