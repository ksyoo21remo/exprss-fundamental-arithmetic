require('dotenv').config()
const fs = require('fs').promises
const path = require('path')
const os = require('os')
const mysql2 = require('mysql2/promise')

async function main() {
  const pool = mysql2.createPool({
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    waitForConnections: true,
    connectionLimit: 2 * os.cpus().length,
    queueLimit: 0
  })

  let musers
  try {
    musers = (await pool.query('SELECT id, name FROM ksyoo.musers'))[0]
  } catch (error) {
    console.error(error)
  }
  
  await pool.end()
}

main()
