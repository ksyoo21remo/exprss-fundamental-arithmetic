require('dotenv').config()
const fs = require('fs').promises
const path = require('path')
const os = require('os')
const mysql2 = require('mysql2/promise')

async function main() {
  const pool = mysql2.createPool({
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    waitForConnections: true,
    connectionLimit: 2 * os.cpus().length,
    queueLimit: 0
  })

  // create insert/surveys_simple.sql
  let musersCount = 0
  const painAreaList = [
    { id: 1, part: '목' },
    { id: 2, part: '좌어깨' },
    { id: 3, part: '우어깨' },
    { id: 4, part: '좌팔꿈치' },
    { id: 5, part: '우팔꿈치' },
    { id: 6, part: '등' },
    { id: 7, part: '허리' },
    { id: 8, part: '좌골반' },
    { id: 9, part: '우골반' },
    { id: 10, part: '좌무릎' },
    { id: 11, part: '우무릎' },
  ]
  const pseudoSurveyResult = []
  try {
    const data = (await pool.query('SELECT COUNT(*) AS cnt FROM ksyoo.musers'))[0]
    musersCount = data[0].cnt
  } catch (error) {
    console.error(error)
  }

  for (let i = 0; i < musersCount; ++i) {
    pseudoSurveyResult.push(painAreaList[Math.floor(Math.random() * painAreaList.length)].id)
  }
  let queryStr = 'INSERT INTO\n  `surveys_simple` (`muser_id`, `painarea`)\nVALUES\n'
  for (let i = 0; i < musersCount; ++i) {
    queryStr = queryStr + '  (' + 
      (i + 1) + ',' + 
      painAreaList[Math.floor(Math.random() * painAreaList.length)].id +
      ')'
    if (i === musersCount - 1) {
      queryStr = queryStr + ';\n'
    }
    else {
      queryStr = queryStr + ',\n'
    }
  }
  try {
    const result = await fs.writeFile(path.join('..', 'insert', 'surveys_simple.sql'), queryStr)
    console.log(result)
  } catch (error) {
    console.error(error)
  }

  // create insert/exercises_simple.sql

  await pool.end()
}

main()
