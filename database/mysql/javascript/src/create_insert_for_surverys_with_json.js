require('dotenv').config()
const fs = require('fs').promises
const path = require('path')
const os = require('os')
const mysql2 = require('mysql2/promise')

async function main() {
  const pool = mysql2.createPool({
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    waitForConnections: true,
    connectionLimit: 2 * os.cpus().length,
    queueLimit: 0
  })

  let musers
  try {
    musers = (await pool.query('SELECT id, name FROM ksyoo.musers'))[0]
  } catch (error) {
    console.error(error)
  }

  // create insert/surveys_with_json.sql
  const painAreaList = [
    { id: 1, part: '목' },
    { id: 2, part: '좌어깨' },
    { id: 3, part: '우어깨' },
    { id: 4, part: '좌팔꿈치' },
    { id: 5, part: '우팔꿈치' },
    { id: 6, part: '등' },
    { id: 7, part: '허리' },
    { id: 8, part: '좌골반' },
    { id: 9, part: '우골반' },
    { id: 10, part: '좌무릎' },
    { id: 11, part: '우무릎' },
  ]
  const pseudoSurveys = []
  for (let x = 0; x < musers.length; ++x) {
    const painAreas = []
    for (let y = 0; y < Math.floor(Math.random() * painAreaList.length); ++y ) {
      let newPainArea = painAreaList[Math.floor(Math.random() * painAreaList.length)].id
      while(painAreas.includes(newPainArea)) {
        newPainArea = painAreaList[Math.floor(Math.random() * painAreaList.length)].id
      }
      painAreas.push(newPainArea)
    }
    pseudoSurveys.push(painAreas)
  }

  let queryStr = 'INSERT INTO\n  `surveys_with_json` (`muser_id`, `pain_areas`)\nVALUES\n'
  for (let i = 0; i < musers.length; ++i) {
    queryStr = queryStr + '  (' + musers[i].id + ', \'' + JSON.stringify(pseudoSurveys[i]) + '\')'
    if (i === musers.length - 1) {
      queryStr = queryStr + ';\n'
    } else {
      queryStr = queryStr + ',\n'
    }
  }
  try {
    await fs.writeFile(path.join('..', 'insert', 'surveys_with_json.sql'), queryStr)
  } catch (error) {
    console.error(error)
  }

  await pool.end()
}

main()
