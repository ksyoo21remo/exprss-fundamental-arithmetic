DROP TABLE IF EXISTS `surveys_simple`;

CREATE TABLE `surveys_simple` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `muser_id` bigint unsigned NOT NULL,
  `painarea` int unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `MUSER_ID` (`muser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
