DROP TABLE IF EXISTS `pain_areas`;

CREATE TABLE `pain_areas` (
  `muser_id` bigint unsigned NOT NULL,
  `pain_area` int unsigned NOT NULL,
  PRIMARY KEY (`muser_id`,`pain_area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
