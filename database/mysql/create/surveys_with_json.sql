DROP TABLE IF EXISTS `surveys_with_json`;

CREATE TABLE `surveys_with_json` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `muser_id` bigint unsigned NOT NULL,
  `pain_areas` json DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
