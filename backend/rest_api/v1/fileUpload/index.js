const express = require('express')
const multer = require('multer')
const path = require('path')

const fileUploadRouter = express.Router()
const upload = multer({
  storage: multer.diskStorage({
    destination: (req, file, done) => {
      done(null, 'uploads')
    },
    filename: (req, file, done) => {
      const ext = path.extname(file.originalname)
      done(null, path.basename(file.originalname, ext) + '_' + Date.now() + ext)
    }
  })
})

fileUploadRouter.post('/', upload.array('uploads'), (req, res) => {
  res.send('OK')
})

module.exports = fileUploadRouter
