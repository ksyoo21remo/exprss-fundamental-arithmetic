const { isTypeDivide, divide } = require('./divide')
const { isTypeMinus, minus } = require('./minus')
const { isTypeMultiply, multiply } = require('./multiply')
const { isTypePlus, plus } = require('./plus')

function calculateAsType(data) {
  const type = data.type
  const a = data.a
  const b = data.b
  let sendData = data.sendData
  if (isTypePlus(type)) {
    sendData.result = plus(a, b)
  }
  else if (isTypeMinus(type)) {
    sendData.result = minus(a, b)
  }
  else if (isTypeMultiply(type)) {
    sendData.result = multiply(a, b)
  }
  else if (isTypeDivide(type)) {
    sendData.result = divide(a, b)
  }
  else {
    sendData = { ...sendData, error: 'wrong-operator' }
  }
  return sendData
}

module.exports = {
  calculateAsType
}
