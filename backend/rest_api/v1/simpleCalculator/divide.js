function isTypeDivide(type) {
  return ['div', 'divide'].includes(type)
}

function divide(a, b) {
  return a / b
}

module.exports = {
  isTypeDivide,
  divide
}
