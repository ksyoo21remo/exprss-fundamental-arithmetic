function isTypeMultiply(type) {
  return ['mul', 'multiply'].includes(type)
}

function multiply(a, b) {
  return a * b
}

module.exports = {
  isTypeMultiply,
  multiply
}
