function isTypeMinus(type) {
  return ['sub', 'substract', 'minus'].includes(type)
}

function minus(a, b) {
  return a - b
}

module.exports = {
  isTypeMinus,
  minus
}
