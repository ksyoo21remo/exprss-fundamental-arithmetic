const express = require('express')
const { calculateAsType } = require('./utils')

const simpleCalculator = express.Router()

simpleCalculator.get('/', (_, res) => {
  const sendHtml = `
    <h1>사칙연산 계산기</h1>
  `
  res.send(sendHtml)
})

simpleCalculator.get('/:type', (req, res) => {
  const type = req.params.type
  const { a, b } = req.query
  console.log(`GET type: ${type} ... a = ${a}, b = ${b}`)

  let sendData = { error: null, result: null }
  if (isNaN(a) || isNaN(b)) {
    return res.json({...sendData, error: 'wrong-operand' })
  }
  sendData = calculateAsType({
    type,
    a: Number(a),
    b: Number(b),
    sendData
  })
  res.send(sendData)
})

simpleCalculator.post('/calculate', (req, res) => {
  const { type, a, b } = req.body
  console.log(`POST type: ${type} ... a = ${a}, b = ${b}`)
  
  let sendData = { error: null, result: null }
  if (isNaN(a) || isNaN(b)) {
    return res.json({...sendData, error: 'wrong-operand' })
  }
  sendData = calculateAsType({
    type,
    a: Number(a),
    b: Number(b),
    sendData
  })
  res.send(sendData)
})

module.exports = simpleCalculator
