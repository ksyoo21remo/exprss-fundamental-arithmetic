function isTypePlus(type) {
  return ['add', 'plus'].includes(type)
}

function plus(a, b) {
  return a + b
}

module.exports = {
  isTypePlus,
  plus
}
