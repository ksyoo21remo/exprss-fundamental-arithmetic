const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const simpleCalculator = require('./rest_api/v1/simpleCalculator')
const fileUploadRouter = require('./rest_api/v1/fileUpload')
const objectBasedGraphql = require('./graphql/objectBased')
const stringBasedGraphql = require('./graphql/stringBased')

const app = express()

app
  .use(logger('dev'))
  .use(express.json())
  .use(express.urlencoded({ extended: false }))
  .use(cookieParser())
  .use(express.static(path.join(__dirname, '..', 'frontend', 'build')))

app
  .use('/v1/simple-calculator', simpleCalculator)
  .use('/v1/file-upload', fileUploadRouter)
  .use('/graphql/object-based', objectBasedGraphql)
  .use('/graphql/string-based', stringBasedGraphql)
  .use('*', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'frontend', 'build', 'index.html'))
  })

module.exports = app
