function multiply(args) {
  if (isNaN(args.a) || isNaN(args.b)) {
    return { error: 'wrong-operand', result: null }
  }
  const a = Number(args.a)
  const b = Number(args.b)
  return { error: null, result: a * b }
}

module.exports = multiply
