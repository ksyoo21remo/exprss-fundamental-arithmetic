const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList
} = require('graphql')
const plus = require('../utils/plus')
const minus = require('../utils/minus')
const multiply = require('../utils/multiply')
const divide = require('../utils/divide')

const rootQueryType = new GraphQLObjectType({
  name: 'Query',
  description: 'Root Query',
  fields: () => ({
    calculate: {
      type: SendMultipleResultType,
      description: '사칙연산 결과값 4개',
      args: {
        a: { type: GraphQLNonNull(GraphQLString) },
        b: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => {
        let sendData = {
          error: null,
          plus: null,
          minus: null,
          multiply: null,
          divide: null
        }
        if (isNaN(args.a) || isNaN(args.b)) {
          return { ...sendData, error: 'error-operand' }
        }
        return {
          ...sendData,
          plus: plus(args).result,
          minus: minus(args).result,
          multiply: multiply(args).result,
          divide: divide(args).result
        }
      }
    },
    plus: {
      type: SendSingleResultType,
      description: '더하기',
      args: {
        a: { type: GraphQLNonNull(GraphQLString) },
        b: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => plus(args)
    },
    minus: {
      type: SendSingleResultType,
      description: '빼기',
      args: {
        a: { type: GraphQLNonNull(GraphQLString) },
        b: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => minus(args)
    },
    multiply: {
      type: SendSingleResultType,
      description: '곱하기',
      args: {
        a: { type: GraphQLNonNull(GraphQLString) },
        b: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => multiply(args)
    },
    divide: {
      type: SendSingleResultType,
      description: '나누기',
      args: {
        a: { type: GraphQLNonNull(GraphQLString) },
        b: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => divide(args)
    },
  })
})

const SendSingleResultType = new GraphQLObjectType({
  name: 'SendSingleResultType',
  description: `JSON { error: string, result: string }
현재까지 가능한 error는 error-operand가 있다`,
  fields: () => ({
    error: { type: GraphQLString },
    result: { type: GraphQLString }
  })
})

const SendMultipleResultType = new GraphQLObjectType({
  name: 'SendMultipleResultType',
  description: `JSON { error: string, result: [string] }
현재까지 가능한 error는 error-operand가 있다`,
  fields: () => ({
    error: { type: GraphQLString },
    plus: { type: GraphQLString },
    minus: { type: GraphQLString },
    multiply: { type: GraphQLString },
    divide: { type: GraphQLString }
  })
})

module.exports = rootQueryType
