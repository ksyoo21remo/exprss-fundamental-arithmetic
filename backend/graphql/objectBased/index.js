const { graphqlHTTP } = require('express-graphql')
const {
  GraphQLSchema
} = require('graphql')
const rootQueryType = require('./query')

const schema = new GraphQLSchema({
  query: rootQueryType
})

const objectBasedGraphql = new graphqlHTTP({
  schema: schema,
  customFormatErrorFn: (error) => ({
    message: error.message,
    locations: error.locations,
    stack: error.stack ? error.stack.split('\n') : [],
    path: error.path,
  }),
  graphiql: true
})

module.exports = objectBasedGraphql
