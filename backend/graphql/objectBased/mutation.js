const { GraphQLObjectType } = require('graphql')

const rootMutationType = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Root Mutation'
})

// module.exports = rootMutationType
