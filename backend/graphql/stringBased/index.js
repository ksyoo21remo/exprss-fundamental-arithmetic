const { graphqlHTTP } = require('express-graphql')
const resolvers = require('./root')
const schema = require('./schema')

const stringBasedGraphql = graphqlHTTP({
  schema: schema,
  rootValue: resolvers,
  customFormatErrorFn: (error) => ({
    message: error.message,
    locations: error.locations,
    stack: error.stack ? error.stack.split('\n') : [],
    path: error.path,
  }),
  graphiql: true
})

module.exports = stringBasedGraphql
