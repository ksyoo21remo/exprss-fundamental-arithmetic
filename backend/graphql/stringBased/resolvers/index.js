function concatStrings(args) {
  const { arrayOfStrings, delim } = args

  console.log(`typeof arrayOfString: ${typeof arrayOfStrings}, typeof delim: ${typeof delim}`)
  arrayOfStrings.forEach((element, index) => console.log(`index: ${index}, element: ${element}`));
  console.log({ arrayOfStrings, delim })

  resultString = 'I am result-string'
  return resultString
}

module.exports = {
  concatStrings
}
