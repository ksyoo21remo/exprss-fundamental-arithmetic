const plus = require('../utils/plus')
const minus = require('../utils/minus')
const multiply = require('../utils/multiply')
const divide = require('../utils/divide')
const { concatStrings } = require('./resolvers')

const resolvers = {
  calculate: (args) => {
    const sendData = {
      error: null,
      plus: null,
      minus: null,
      multiply: null,
      divide: null
    }
    if (isNaN(args.a) || isNaN(args.b)) {
      return { ...sendData, error: 'error-operand' }
    }
    return {
      ...sendData,
      plus: plus(args).result,
      minus: minus(args).result,
      multiply: multiply(args).result,
      divide: divide(args).result,
    }
  },
  plus: (args) => plus(args),
  minus: (args) => minus(args),
  multiply: (args) => multiply(args),
  divide: (args) => divide(args),

  concatStrings: (args) => concatStrings(args)
}

module.exports = resolvers
