const { buildSchema } = require('graphql')

const schema = buildSchema(`
  type Query {
    calculate(a: String!, b: String!): SendManyResultsType
    plus(a: String!, b: String!): SendSingleResultType
    minus(a: String!, b: String!): SendSingleResultType
    multiply(a: String!, b: String!): SendSingleResultType
    divide(a: String!, b: String!): SendSingleResultType

    concatStrings(arrayOfStrings: [String!]!, delim: String): String
  }

  type SendSingleResultType {
    error: String
    result: String
  }

  type SendManyResultsType {
    error: String,
    plus: String,
    minus: String,
    multiply: String,
    divide: String,
  }
`)

module.exports = schema
