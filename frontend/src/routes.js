import Home from './components/Home'
import UsingRestApi, { UsingRestApiHome } from './components/UsingRestApi'
import GetCalculator from './components/UsingRestApi/GetCalculator'
import PostCalculator from './components/UsingRestApi/PostCalculator'
import UsingGraphql, { UsingGraphqlHome } from './components/UsingGraphql'
import ObjectBasedCalculator from './components/UsingGraphql/ObjectBasedCalculator'
import StringBasedCalculator from './components/UsingGraphql/StringBasedCalculator'
import FileUpload, { FileUploadHome } from './components/FileUpload'
import FileUploadSuccess from './components/FileUpload/success'
import FileUploadFailure from './components/FileUpload/failure'

const routes = [
  { path: '/', element: <Home /> },
  {
    path: 'using-rest-api', element: <UsingRestApi />,
    children: [
      { path: '', element: <UsingRestApiHome /> },
      { path: 'get-calculator', element: <GetCalculator /> },
      { path: 'post-calculator', element: <PostCalculator /> }
    ]
  },
  {
    path: 'using-graphql', element: <UsingGraphql />,
    children: [
      { path: '', element: <UsingGraphqlHome /> },
      { path: 'object-based', element: <ObjectBasedCalculator /> },
      { path: 'string-based', element: <StringBasedCalculator /> }
    ]
  },
  {
    path: 'file-upload', element: <FileUpload />,
    children: [
      { path: '', element: <FileUploadHome /> },
      { path: 'success', element: <FileUploadSuccess /> },
      { path: 'failure', element: <FileUploadFailure /> },
    ]
  },
  { path: '*', element: <h1>404 Not Found</h1> }
]

export default routes
