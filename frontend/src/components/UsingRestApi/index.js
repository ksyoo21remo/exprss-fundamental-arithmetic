import { Outlet } from 'react-router'
import { Link } from 'react-router-dom'

export default function UsingRestApi() {
  return <>
    <Outlet />
  </>
}

export function UsingRestApiHome() {
  return <>
    <h1>REST API 계산기</h1>
    <div>
      <Link to='get-calculator'>
        <button>
          <h1>GET 계산기</h1>
        </button>
      </Link>
    </div>
    <div>
      <Link to='post-calculator'>
        <button>
          <h1>POST 계산기</h1>
        </button>
      </Link>
    </div>
  </>
}
