import axios from 'axios'
import { useState } from 'react'

export default function GetCalculator() {
  const [a, setA] = useState('0')
  const [b, setB] = useState('0')
  const [type, setType] = useState('plus')
  const [result, setResult] = useState('null')

  function handleChangeA(e) {
    e.preventDefault()
    setA(e.target.value)
  }

  function handleChangeType(e) {
    e.preventDefault()
    setType(e.target.value)
  }

  function handleChangeB(e) {
    e.preventDefault()
    setB(e.target.value)
  }

  async function handleSubmit(e) {
    e.preventDefault()
    const queryUrl = `/v1/simple-calculator/${type}`
    try {
      const response = (await axios({
        method: 'GET',
        url: queryUrl,
        params: { a, b }
      })).data
      if (response.error) {
        console.error(response.error)
      }
      setResult(response.result)
    } catch (error) {
      console.error(error)
    }
  }

  const bigFont = { fontSize: '2em' }

  return <>
    <h1>[ GET 계산기 ]</h1>
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor='valA'></label>
        <input
          type='number'
          name='valA'
          value={a}
          onChange={handleChangeA}
          style={bigFont}
        />
      </div>
      <div>
        <select
          name='select_type'
          onChange={handleChangeType}
          style={bigFont}
        >
          <option value='plus' label='+' />
          <option value='minus' label='−' />
          <option value='multiply' label='×' />
          <option value='divide' label='÷' />
        </select>
      </div>
      <div>
        <label htmlFor='valB'></label>
        <input
          type='number'
          name='valB'
          value={b}
          onChange={handleChangeB}
          style={bigFont}
        />
      </div>
      <div>
        <button
          type='submit'
          style={bigFont}
        >
          =
        </button>
      </div>
    </form>
    <div
      style={bigFont}
    >
      {result}
    </div>
  </>
}
