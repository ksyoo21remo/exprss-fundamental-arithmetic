import { Link } from 'react-router-dom'

export default function Home() {
  return <>
    <div>
      <Link to='/using-rest-api'>
        <button>
          <h1>REST API 사용하기</h1>
        </button>
      </Link>
    </div>
    <div>
      <Link to='/using-graphql'>
        <button>
          <h1>GraphQL 사용하기</h1>
        </button>
      </Link>
    </div>
    <div>
      <Link to='/file-upload'>
        <button>
          <h1>파일 업로드하기</h1>
        </button>
      </Link>
    </div>
  </>
}
