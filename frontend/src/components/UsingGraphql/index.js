import { Outlet } from 'react-router'
import { Link } from 'react-router-dom'

export default function UsingGraphql() {
  return <>
    <Outlet />
  </>
}

export function UsingGraphqlHome() {
  return <>
    <h1>GraphQL 계산기</h1>
    <div>
      <Link to='object-based'>
        <button>
          <h1>Object Based GraphQL 계산기</h1>
        </button>
      </Link>
    </div>
    <div>
      <Link to='string-based'>
        <button>
          <h1>String Based GraphQL 계산기</h1>
        </button>
      </Link>
    </div>
  </>
}
