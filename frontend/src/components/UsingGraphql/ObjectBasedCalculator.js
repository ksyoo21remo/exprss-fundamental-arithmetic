import axios from 'axios'
import { useState } from 'react'

export default function ObjectBasedCalculator() {
  const [a, setA] = useState('0')
  const [b, setB] = useState('0')
  const [resultPlus, setResultPlus] = useState(null)
  const [resultMinus, setResultMinus] = useState(null)
  const [resultMultiply, setResultMultiply] = useState(null)
  const [resultDivide, setResultDivide] = useState(null)

  function handleChangeA(e) {
    e.preventDefault()
    setA(e.target.value)
  }

  function handleChangeB(e) {
    e.preventDefault()
    setB(e.target.value)
  }

  async function handleSubmit(e) {
    e.preventDefault()
    const queryUrl = '/graphql/object-based'
    const query = `query X($a: String!, $b: String!) {
      plus(a: $a, b: $b) {
        error, result
      }
      minus(a: $a, b: $b) {
        error, result
      }
      multiply(a: $a, b: $b) {
        error, result
      }
      divide(a: $a, b: $b) {
        error, result
      }
    }`
    const variables = { a, b }
    try {
      const response = (await axios({
        method: 'POST',
        url: queryUrl,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        data: {
          query,
          variables
        }
      })).data
      if (response.error) {
        console.error(response.error)
      }
      const results = response.data
      setResultPlus(results.plus.result)
      setResultMinus(results.minus.result)
      setResultMultiply(results.multiply.result)
      setResultDivide(results.divide.result)
    } catch (error) {
      console.error(error)
    }
  }

  const bigFont = { fontSize: '2em' }

  return <>
    <h1>[ 객체 기반 GraphQL 계산기 ]</h1>
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor='valA' style={bigFont}>첫번째 피연산자: </label>
        <input
          type='number'
          name='valA'
          value={a}
          onChange={handleChangeA}
          style={bigFont}
        />
      </div>
      <div>
        <label htmlFor='valB' style={bigFont}>두번째 피연산자: </label>
        <input
          type='number'
          name='valB'
          value={b}
          onChange={handleChangeB}
          style={bigFont}
        />
      </div>
      <div>
        <button
          type='submit'
          style={bigFont}
        >
          =
        </button>
      </div>
    </form>
    <div>
      <div style={bigFont}>
        더하기 결과: {resultPlus}
      </div>
      <div style={bigFont}>
        빼기 결과: {resultMinus}
      </div>
      <div style={bigFont}>
        곱하기 결과: {resultMultiply}
      </div>
      <div style={bigFont}>
        나누기 결과: {resultDivide}
      </div>
    </div>
  </>
}
