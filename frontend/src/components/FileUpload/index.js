import axios from 'axios'
import { useState } from 'react'
import { Outlet } from 'react-router'
import { useNavigate } from 'react-router-dom'

export default function FileUpload() {
  return <Outlet />
}

export function FileUploadHome() {
  const [selectedFiles, setSelectedFiles] = useState(null)
  const navigate = useNavigate()

  function handleSelectedFiles(e) {
    e.preventDefault()
    setSelectedFiles(e.target.files)
  }

  async function handleUpload(e) {
    e.preventDefault()
    const data = new FormData()
    data.append('uploads', selectedFiles)
    const url = '/v1/file-upload'
    try {
      const response = (await axios({
        method: 'POST',
        url,
        timeout: 3 * 60 * 60,
        data
      })).data
      if (response === 'OK') {
        return navigate('success')
      }
    } catch (error) {
      console.error(error)
    }
    navigate('failure')
  }

  const style = { fontSize: '2em', padding: '0.5em' }
  const padding = { padding: '1em' }

  return <>
    <div>
      <form onSubmit={handleUpload}>
        <div>
          <label htmlFor='uploads' style={style}>
            ↓↓↓ 업로드 할 파일 선택 ↓↓↓ (여러개 선택 가능)
          </label>
        </div>
        <div>
          <input
            type='file'
            name='uploads'
            onChange={handleSelectedFiles}
            multiple
            style={style}
          />
        </div>
        <div style={padding}>
          <button type='submit' style={style}>업로드</button>
        </div>
      </form>
      {/* <form action='/v1/file-upload' method='POST' enctype='multipart/form-data'>
        <div>
          <input type='file' name="uploads" style={style} multiple />
        </div>
        <div style={padding}>
          <button type='submit' style={style}>업로드</button>
        </div>
      </form> */}
    </div>
  </>
}
